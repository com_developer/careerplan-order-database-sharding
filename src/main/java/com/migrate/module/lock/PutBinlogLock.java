package com.migrate.module.lock;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 锁竞争类对象
 *
 * @author zhonghuashishan
 */
public class PutBinlogLock {

    private final AtomicBoolean putMessageSpinLock = new AtomicBoolean(true);

    /**
     * 上锁
     */
    public void lock() {
        boolean flag;
        do {
            flag = this.putMessageSpinLock.compareAndSet(true, false);
        }
        while (!flag);
    }

    /**
     * 解锁
     */
    public void unlock() {
        this.putMessageSpinLock.compareAndSet(false, true);
    }
}
